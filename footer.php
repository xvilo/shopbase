		<footer class="page-footer grey-border__top">
			<nav class="page-footer--nav">
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
			</nav>
			<div class="page-foter--copyright">
				&copy; <?php echo date('Y')?> - <a href="https://tropicaljuice.nl?utm_source=deurcamerawinkel&utm_medium=footer%20link&utm_campaign=made%20by%20links" target="_blank">Tropical Juice</a>
			</div>
		</footer>
		<?php wp_footer() ?>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-78984125-1', 'auto');
			ga('send', 'pageview');
		</script>
	</body>
</html>