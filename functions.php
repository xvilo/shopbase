<?php	
	add_filter( 'ot_theme_mode', '__return_true' );
	require __DIR__ . '/vendor/autoload.php';
	require __DIR__ . '/theme-options.php';
	
	add_theme_support( 'title-tag' );
	
	function registreer_menus() {
		register_nav_menus(
			array(
				'footer-menu' => __( 'Footer menu' ),
			)
		);
	}
	add_action( 'init', 'registreer_menus' );

	function TJGetLogo(){
		return ot_get_option('logo_svg');
	}