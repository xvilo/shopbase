<!doctype html>
<html lang="nl">
	<head>
		<meta charset="utf-8">
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php wp_head() ?>
		
		<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/style.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<meta name="viewport" content="initial-scale=1.0, width=device-width" />		
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
		<header class="header">
			<div class="header__grey-bg header--top-bar grey-border__bottom">
				<div class="header--logo animated">
					<h1 class="header--logo__svg"><a href="<?php echo home_url() ?>"><?php echo TJGetLogo(); ?></a></h1>
				</div>
				<div class="search">
					<form role="search" method="get" id="searchform" class="searchform" action="https://deurcamerawinkel.nl/">
					<input class="search--input" placeholder="Zoeken..." name="s" id="s">
					<button id="searchsubmit" type="submit" class="search--buton" name="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
					</form>
				</div>
				<div class="meta">
					<ul class="meta--list">
						<li class="meta--list-item"><a class="btn btn__orange btn__small" href="#">Registreren</a></li>
						<li class="meta--list-item"><a class="meta--link" href="#">Inloggen</a></li>
						<li class="meta--list-item"><a class="meta--link" href="/cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="header--usp grey-border__bottom">
				<ul class="usp--list">
					<li class="usp--list-item">Snelle levering door heel Nederland</li>
					<li class="usp--list-item">Deskundige Klantenservice</li>
					<li class="usp--list-item">Scherpe prijzen</li>
					<li class="usp--list-item__featured"><i class="fa fa-lock" aria-hidden="true"></i> Veilig betalen</li>
				</ul>
			</div>
		</header>