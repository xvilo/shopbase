<?php get_header(); ?>
<section class="container">
	<main>
		 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		 	<h2 class="entry--title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		 	<div class="entry">
		 		<?php the_content(); ?>
		 	</div>
		 	</div> 
		 <?php endwhile; else : ?>
		 	<h2 class="entry--title">404 - Not found 😅</h2>
		 	<p><?php _e( 'Sorry, you\'ve stumbled upon an empty page 😭.' ); ?></p>
		 <?php endif; ?>
	</main>
	<aside id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
				<?php dynamic_sidebar( 'sidebar-main' ); ?>
		<?php endif; ?>
	</aside>
</section>
<?php get_footer(); ?>